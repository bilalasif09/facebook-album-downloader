module HomeHelper

	def self.logged_in?
		return !session[:token].nil?
	end

	def self.get_post_details post_id, graph
		return graph.get_object(post_id)
	end

	def self.get_profile_pic user_id
		return "http://graph.facebook.com/#{user_id}/picture?height=64&width=64"
	end

	def self.compress dir
		zipfile_name = File.join( dir, File.basename(dir) ) + '.zip'

		Zip::File.open( zipfile_name, Zip::File::CREATE) do |zipfile|
			Dir[File.join( dir, '**', '**')].each do |file|
				puts "File being added to zip ====> #{file}\n"
				zipfile.add( file.sub( dir, ''), file)
			end
		end

		zipfile_name
	end

end
