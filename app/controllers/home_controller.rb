require 'zip'

class HomeController < ApplicationController

	@@app_settings = {
		:app_id => '553554854707855',
		:app_secret => 'e468ba487e77030017bc03503dbaca2f',
		:callback_url => 'http://localhost:3000/home/validate',
		:permissions => 'publish_stream,read_stream'
	}

	@@mcdalli = Dalli::Client.new


	def index
	end

	def fb_photos
		urls=params[:my_urls]
		total_urls=params[:total_urls]
		path = Dir.pwd
		
		if params[:name]
			uname = params[:name] + Time.now.to_i.to_s
		end

		puts "---->", path, urls, uname, total_urls

		Dir.mkdir(File.join(path, "public/fb_dloads/#{uname}"))
		total_urls=total_urls.to_i

		i=0
		begin
			filename=File.basename("#{urls[i]}")
			system ("wget #{urls[i]} -O public/fb_dloads/#{uname}/#{filename}")
			i +=1
		end	while i<total_urls

		puts "here is the url link---> #{urls[1]}"

		path = Dir.pwd + "/public/fb_dloads/#{uname}/"
		callback = params[:callback]
		download_zip path, callback

	end

	def download_zip path, callback
		zip_name = HomeHelper.compress(path)
		public_folder = Dir.pwd + "/public/fb_dloads/"
		zip_name.sub!( public_folder, '' )
		session[:zip_name] = zip_name
		render json: {zip_name: zip_name}.to_json, callback: params[:callback]
	end

	def save_zip 
		zip_name = session[:zip_name]
		send_file Rails.root.join('public/fb_dloads', "#{zip_name}"), :type=>"application/zip", :x_sendfile=>true
	end

#	def index
#		token = session[:token]
#		puts "Home token = #{token}"
#		logged_in = !session[:token].nil?

#		if logged_in
#			puts "logged in"
#			graph = Koala::Facebook::API.new(token)
#			@user = session[:user]
#			@feed = @@mcdalli.get('feed_key')
#
#			puts @user.inspect
#
#			if @feed.nil?
#				@feed = graph.fql_query("SELECT type, created_time, post_id, actor_id, message, description, comment_info, like_info FROM stream WHERE filter_key = 'nf' LIMIT 15")
#				puts "here is the feed"
#
#				@feed.each_with_index do |f,i|
#					# ignor comment created posts
#					if f['type'] == 257
#						@feed.delete_at i
#						next
#					end
#
#					post_id = f['post_id']
#					puts "your post id's are #{post_id}"
#
#					begin
#						f['details'] = HomeHelper::get_post_details post_id, graph
#						f['profile_pic'] = HomeHelper::get_profile_pic f['actor_id']
#
#						puts "\n\nProfile pic #{f['profile_pic']}\n\n"
#					rescue Exception => e
#						puts "Error fetching details for post, deleting from feed #{post_id}"
#						puts e.inspect
#						puts e.message
#						@feed.delete_at i
#					end
#				end
#				@@mcdalli.set('feed_key', @feed)
#			end
#
#		end
#	end

	def validate 
		oauth = Koala::Facebook::OAuth.new(@@app_settings[:app_id], @@app_settings[:app_secret], @@app_settings[:callback_url])
		code=params['code']
		puts "code = #{code}"
		token=oauth.get_access_token(code)
		puts "Token #{token.inspect}"

		session[:token] = token
		graph = Koala::Facebook::API.new(token)
		session[:user] = graph.get_object('me')

		redirect_to '/'
	end

	def logout 
		session[:token] = nil
		redirect_to '/'
	end

	def profile
	@user = session[:user]
	end

	def login
		@oauth = Koala::Facebook::OAuth.new(@@app_settings[:app_id], @@app_settings[:app_secret], @@app_settings[:callback_url])
		url_for_oauth = @oauth.url_for_oauth_code(:permissions	=> @@app_settings[:permissions])
		redirect_to url_for_oauth
	end

	def b_day

		token=session[:token]
		@graph = Koala::Facebook::API.new(token)
		@feed = @graph.get_connection('me','feed')
		@feed_key=@feed	
	end

	def photos
		token=session[:token]
		@graph = Koala::Facebook::API.new(token)
		@feed = @graph.get_connection('me','feed')
		@feed_key=@feed
	end


	def reply
		token=session[:token]
		puts "here is reply token"
		puts @token.inspect
		@graph = Koala::Facebook::API.new(token)
		post_id = params[:post_id]
		comment = params[:comment]
		puts "here is the comment"
		puts comment
		puts post_id
		@graph.put_comment(post_id, comment)
		"Posted '#{comment}' on Post '#{post_id}'"
	end

	def b_day_like
		token=session[:token]
		puts "adf"
		@graph = Koala::Facebook::API.new(token)
		#	if @feed_key.nil?
		#	@feed = @graph.get_connection('me','feed')
		#	settings.cache.set('feed_key', @feed)
		#	@feed_key = @mcdalli.get('feed_key')
		#	end
		@feed_key.each do |f| 
			if f['type']=='status'
				if f['message']=="bla bla bla"
					@graph.put_like(f['id'])
				end 
			end
		end

	end

	def b_day_comment
		token=session[:token]
		puts "adf"
		graph = Koala::Facebook::API.new(token)
		#	if @feed_key.nil?
		#	@feed = @graph.get_connection('me','feed')
		#	settings.cache.set('feed_key', @feed)
		#	@feed_key = settings.cache.get('feed_key')
		#	end
		feed=graph.get_connection('me','feed')
		@feed_key=feed
		comment=params[:comment]
		@feed_key.each do |f| 
			if f['type']=='status'
				if f['message']=="bla bla bla"
					@graph.put_comment(f['id'],comment)
				end 
			end
		end

	end

end
